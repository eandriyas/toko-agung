<tr>
		<p>Alamat pengiriman</p>
	<td>

		<?php

		function getPlace($a){
			if ($a=='all') {
				$link = "http://api.rajaongkir.com/starter/province";
			} else {
				$link = "http://api.rajaongkir.com/starter/city?province=".$a."";
			}
			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => $link ,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => array(
					"key: cef8ac9b9eb9227160589fb18a875d49"
					),
				));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
				echo "cURL Error #:" . $err;
			} else {
				$hasil = json_decode($response);
		// print_r($hasil->rajaongkir->results);
				$akhir = $hasil->rajaongkir->results;
				?>
				<?php
				if ($a=='all') { ?>

				<?php foreach ($akhir as $provinsi) { ?>
				<option value="<?php echo $provinsi->province_id ?>"><?php echo $provinsi->province; ?></option>

				<?php } ?>
				<?php } else { ?>

				<?php foreach ($akhir as $city) { ?>
				<option value="<?php echo $city->city_id ?>"><?php echo $city->city_name; ?></option>

				<?php } ?>
				<?php } ?>
				<?php
			}
		}
		?>
		<script type="text/javascript" src="jquery-2.1.4.min.js"></script>

		<label style="margin-top:10px;" for="provinsi">Provinsi</label>
		<select style="margin-top:10px;margin-bottom:5px;mid-width:100px;" id="Provinsi" name="provinsi">
			<option value="0"></option>
			<?php 
	//menampilkan semua provinsi yang ada diindonesia
			getPlace('all'); ?>

		</select>

		<label style="margin-top:10px;" for="kab">Kabupaten/Kota</label><br>
		<select disabled style="margin-top:10px;margin-bottom:5px;min-width:100px;" id="Kabupaten" name="kab">
			<option value="0"></option>
			<?php //getPlace(5); ?>

		</select>

		<br>
		<label style="margin-top:10px;" for="pos">Kode Pos</label><br>
		<input style="margin-top:10px;margin-bottom:5px;min-width:100px;" type="text" name="pos" id="kodePos">

		
		<br>
		<label style="margin-top:10px;" for="biaya">Alamat</label><br>
		<textarea name="alamat" id="" cols="30" rows="5"></textarea>
		<br>
		<br>
		<label style="margin-top:10px;" for="jasa">Jasa Pengiriman</label><br>
		<select id="Jasa" style="margin-top:10px;margin-bottom:5px;min-width:100px;" id="Jasa" name="jasa">
			<option value="0"></option>
			<option value="jne">JNE</option>
			<option value="pos">POS</option>
			<option value="tiki">TIKI</option>	
		</select>
		<br>
		<label for="berat">Berat Barang</label>
		<p id="beratBarang"> </p>

		<label for="paket" id="paketPengiriman">Paket Pengiriman </label><br>
		<div id="Paket">
			

		</div>

		<br>
		<label style="margin-top:10px;" for="biaya">Total Biaya yang Harus dibayar</label><br>
		<!-- <input style="margin-top:10px;margin-bottom:5px;min-width:100px;" type="text" name="biaya" id="" readonly > -->
		 <p style="color:red; font-size:16pt;"><span id="totalBiayaKirim"></span></p>

	</td>
</tr>

<script type="text/javascript">
// merubah tampilan kabupaten ketika dipilih provinsi
$(document).ready(function(){



	$('#Provinsi').on('change', function(){
		$('#Kabupaten').val('0');
		$('#Jasa').val('0');
		$('#Paket').empty();
		var prov_id = $(this).val();
		$.ajax({
			url: "al_f.php?f=getKabupaten&p="+prov_id,
			contex : document.body
		}).done(function(data){
			$('#Kabupaten option').remove();
			$('#Kabupaten').append(data);
			$('#Kabupaten').prop('disabled', false);

		});
	});
	$('#Kabupaten').on('change', function(){
		var kodepos = $('#Kabupaten option:selected').attr('data-pos');
		$('#kodePos').val(kodepos);
		$('#Jasa').val('0');
	});

	// hitung pengiriman
	$('#Jasa').on('change', function(){
		var jasa = $(this).val();
		var city_id = $('#Kabupaten').val();

		console.log(jasa);
		console.log(city_id);
		// menjumlahkan berat dari barang

		var texts= $(".isi_tabel_nama").map(function() {
			return $(this).attr('data-berat');
		}).get();

		var sum = 0;
		for (var i = 0; i<texts.length; i++) {
			sum += parseFloat(texts[i]);
		};
		console.log(sum);
		$('#beratBarang').empty();
		$('#beratBarang').append('<strong style="color:red;">'+sum+'</strong> gram');
		var dataKirim = {
			origin : 92,
			destination : city_id,
			weight : sum,
			courier : jasa

		}
		console.log(dataKirim);
		$.ajax({
			type : "GET",
			url : "al_f.php?f=getCost&p='hitung'",
			dataType : 'json',
			data: dataKirim,
			success : function(msg){
				if (msg[0].costs=="") {
					alert('Paket pengiriman tidak tersedia');
					window.location.reload();
				};
				// var obj = JSON.parse(msg);
				var a = msg[0].costs;
				$('#Paket').empty();
				for (var i = 0; i < a.length; i++) {
					console.log(a[i]);
					
					$('#Paket').append('<input name="jasa" type="radio" data-name ="'+a[i].service+'" value="'+a[i].cost[0].value+'" /><span>'+a[i].service+'</span> Rp. '+a[i].cost[0].value+'<br>');
				};
				// console.log(msg[0].costs);
			}
		});

	});
	
	
});

function format1(n, currency) {
    return currency + " " + n.toFixed(2).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
    });
}

$(document).on("click", "input:radio[name='jasa']", function() {
//append code here
	var bKirim = $(this).val();
	var totalTanpa = $('#totalTanpa').attr('data-total');

	var hasil = parseFloat(bKirim) + parseFloat(totalTanpa);
	var a = format1(hasil, 'Rp. ');
	$('#totalBiayaKirim').text(a);

	console.log(hasil);

	// provinsi
	var prov = $('#Provinsi option:selected').text();
	// kabupaten
	var kab = $('#Kabupaten option:selected').text();
	// kode pos
	var pos = $('#kodePos').val();
	// alamat
	var alamat = $('textarea[name=alamat]').val();
	// jasa
	var jasa = $('#Jasa option:selected').text();
	// paket
	var paket = $('input:radio[name=jasa]:checked').attr('data-name');;
	// biayaPaket
	var biayaPaket = $('input:radio[name=jasa]:checked').val();
	// total yang harus dibayar
	hasil;

	var dataTotal = {
		provinsi : prov,
		kabupaten : kab,
		pos : pos,
		alamat : alamat,
		jasa : jasa,
		paket : paket,
		biaya_paket : biayaPaket,
		biaya : hasil
	}
	$.ajax({
			type : "GET",
			url : "al_f.php?f=getBiaya&p='hitung'",
			dataType : 'json',
			data: dataTotal,
			success : function(msg){
				console.log(msg);
			}
		});

});




</script>