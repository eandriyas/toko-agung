-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 04, 2015 at 04:13 PM
-- Server version: 5.5.42-37.1
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `yusup300_telorsinbrebes`
--

-- --------------------------------------------------------

--
-- Table structure for table `admintbl`
--

CREATE TABLE IF NOT EXISTS `admintbl` (
  `id` int(11) NOT NULL,
  `username` varchar(35) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admintbl`
--

INSERT INTO `admintbl` (`id`, `username`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3'),
(6, 'sigit', '223a0fa8f15933d622b3c7a13f186027');

-- --------------------------------------------------------

--
-- Table structure for table `barangtbl`
--

CREATE TABLE IF NOT EXISTS `barangtbl` (
  `id` int(11) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `deskripsi` text NOT NULL,
  `warna` varchar(25) NOT NULL,
  `ukuran` varchar(15) NOT NULL,
  `kategori` varchar(25) NOT NULL,
  `harga` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `gambar` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barangtbl`
--

INSERT INTO `barangtbl` (`id`, `nama`, `deskripsi`, `warna`, `ukuran`, `kategori`, `harga`, `stock`, `gambar`) VALUES
(14, 'Telur Asin Rebus', 'Telur Asin yang setelah mengalami proses pengasinan selama seminggu lalu di rebus                           ', '', '', 'Telor Asin Asap', 3000, 1000, 'telurasinrebus.png'),
(15, 'Telor Asin Asap', 'Telur Asin yang setelah mengalami proses pengasinan di asapi di atas tungku api         ', '', '', 'Telor Asin Asap', 3300, 1000, 'telurasinasap.png'),
(16, 'Telur Asin Panggang', 'Telur Asin yang setelah mengalami proses pengasinan lalu di panggang di dalam oven         ', '', '', 'Telor Asin Panggang', 3500, 1000, 'telurasinpanggang.png');

-- --------------------------------------------------------

--
-- Table structure for table `downloadtbl`
--

CREATE TABLE IF NOT EXISTS `downloadtbl` (
  `id` int(11) NOT NULL,
  `judul` text NOT NULL,
  `deskripsi` text NOT NULL,
  `url` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `downloadtbl`
--

INSERT INTO `downloadtbl` (`id`, `judul`, `deskripsi`, `url`) VALUES
(1, 'Katalog Produk 2015', 'Informasi mengenai produk - produk terbaru tahun 2015, lengkap beserta harga, merk tipe, dan lain - lain.', 'download/KATALOG.PDF'),
(2, 'Formulir Reseller', 'Anda dapat menjadi reseller di toko kami dengan mengisi formulir yang bisa Anda download, dan kirim melalui email !!!', 'download/FORMULIR.PDF');

-- --------------------------------------------------------

--
-- Table structure for table `kategoritbl`
--

CREATE TABLE IF NOT EXISTS `kategoritbl` (
  `id` int(11) NOT NULL,
  `kategori` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategoritbl`
--

INSERT INTO `kategoritbl` (`id`, `kategori`) VALUES
(5, 'Telor Asin Rebus'),
(6, 'Telor Asin Asap'),
(7, 'Telor Asin Panggang');

-- --------------------------------------------------------

--
-- Table structure for table `konfirmasitbl`
--

CREATE TABLE IF NOT EXISTS `konfirmasitbl` (
  `no_order` varchar(10) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konfirmasitbl`
--

INSERT INTO `konfirmasitbl` (`no_order`, `nama`, `status`) VALUES
('12', 'Sigit Priyandae', 'Proses Pengiriman'),
('21', 'sigit', 'Terkirim');

-- --------------------------------------------------------

--
-- Table structure for table `newstbl`
--

CREATE TABLE IF NOT EXISTS `newstbl` (
  `tanggal` date NOT NULL,
  `judul` varchar(50) NOT NULL,
  `news` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newstbl`
--

INSERT INTO `newstbl` (`tanggal`, `judul`, `news`) VALUES
('2015-06-24', 'Lowongan Pekerjaan', 'Dibutuhkan sebagai Admin Bungahati, Wanita, Max 30 thn, SMA, Minat: hub 0274 - 886 985. Terima kasih'),
('2015-06-24', 'Diskon', 'Bungahati memberikan diskon besar-besaran pada bulan Ramadhan, dapatkan bonus barang unik.');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggantbl`
--

CREATE TABLE IF NOT EXISTS `pelanggantbl` (
  `id` int(11) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `alamat` text NOT NULL,
  `email` text NOT NULL,
  `telepon` varchar(25) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggantbl`
--

INSERT INTO `pelanggantbl` (`id`, `nama`, `alamat`, `email`, `telepon`, `username`, `password`) VALUES
(15, 'Rian', 'Mancasan Kidul', 'rian@gmail.com', '087772003123', 'rian', 'cb2b28afc2cc836b33eb7ed86f99e65a'),
(16, 'andre', 'concat', '', '', '', 'd41d8cd98f00b204e9800998ecf8427e'),
(17, 'imron', 'tegal', 'imrontegal@gmail.com', '0856482', 'imron', '1aaec45698dc7574b81077fd7192110b'),
(18, 'Abdul', 'Jalan M Husni Thamrin 11 Tegal', 'abdulbrebes@gmail.com', '085678471745', 'Abdul', 'caff97b811b4c26e9f6c0eb4b67d1120');

-- --------------------------------------------------------

--
-- Table structure for table `transaksirincitbl`
--

CREATE TABLE IF NOT EXISTS `transaksirincitbl` (
  `notransaksi` varchar(25) NOT NULL,
  `username` varchar(20) NOT NULL,
  `id` int(11) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksirincitbl`
--

INSERT INTO `transaksirincitbl` (`notransaksi`, `username`, `id`, `nama`, `harga`, `jumlah`, `subtotal`) VALUES
('31', 'rian', 12, 'Cincin LOVE', 270000, 1, 270000),
('32', 'rian', 11, 'Dompet Carizon', 210000, 1, 210000),
('33', 'rian', 10, 'Cincin Emas Series', 400000, 1, 400000),
('34', 'rian', 7, 'Kalung White 1', 375000, 1, 375000),
('34', 'rian', 12, 'Cincin LOVE', 270000, 1, 270000),
('35', 'rian', 9, 'Gelang Brown Ass', 130000, 1, 130000),
('36', 'rian', 11, 'Dompet Carizon', 210000, 1, 210000),
('36', 'rian', 12, 'Cincin LOVE', 270000, 1, 270000),
('37', 'rian', 11, 'Dompet Carizon', 210000, 1, 210000),
('38', 'rian', 11, 'Dompet Carizon', 210000, 1, 210000),
('39', 'rian', 13, 'Dompet Coriso', 150000, 1, 150000),
('40', 'rian', 12, 'Cincin LOVE', 270000, 2, 540000),
('41', 'Abdul', 13, 'Dompet Coriso', 150000, 2, 300000),
('41', 'Abdul', 12, 'Cincin LOVE', 270000, 1, 270000);

-- --------------------------------------------------------

--
-- Table structure for table `transaksitbl`
--

CREATE TABLE IF NOT EXISTS `transaksitbl` (
  `notransaksi` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksitbl`
--

INSERT INTO `transaksitbl` (`notransaksi`, `username`, `status`) VALUES
(31, 'rian', 1),
(32, 'rian', 1),
(33, 'rian', 1),
(34, 'rian', 1),
(35, 'rian', 1),
(36, 'rian', 1),
(37, 'rian', 1),
(38, 'rian', 1),
(39, 'rian', 1),
(40, 'rian', 1),
(41, 'Abdul', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admintbl`
--
ALTER TABLE `admintbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barangtbl`
--
ALTER TABLE `barangtbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `downloadtbl`
--
ALTER TABLE `downloadtbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategoritbl`
--
ALTER TABLE `kategoritbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pelanggantbl`
--
ALTER TABLE `pelanggantbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksitbl`
--
ALTER TABLE `transaksitbl`
  ADD PRIMARY KEY (`notransaksi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admintbl`
--
ALTER TABLE `admintbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `barangtbl`
--
ALTER TABLE `barangtbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `downloadtbl`
--
ALTER TABLE `downloadtbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kategoritbl`
--
ALTER TABLE `kategoritbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `pelanggantbl`
--
ALTER TABLE `pelanggantbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `transaksitbl`
--
ALTER TABLE `transaksitbl`
  MODIFY `notransaksi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
