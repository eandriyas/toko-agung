-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 06, 2015 at 08:17 PM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yusup300_telorsinbrebes`
--

-- --------------------------------------------------------

--
-- Table structure for table `admintbl`
--

CREATE TABLE `admintbl` (
  `id` int(11) NOT NULL,
  `username` varchar(35) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admintbl`
--

INSERT INTO `admintbl` (`id`, `username`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3'),
(6, 'sigit', '223a0fa8f15933d622b3c7a13f186027');

-- --------------------------------------------------------

--
-- Table structure for table `barangtbl`
--

CREATE TABLE `barangtbl` (
  `id` int(11) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `deskripsi` text NOT NULL,
  `berat` int(11) NOT NULL,
  `warna` varchar(25) NOT NULL,
  `ukuran` varchar(15) NOT NULL,
  `kategori` varchar(25) NOT NULL,
  `harga` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `gambar` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barangtbl`
--

INSERT INTO `barangtbl` (`id`, `nama`, `deskripsi`, `berat`, `warna`, `ukuran`, `kategori`, `harga`, `stock`, `gambar`) VALUES
(18, 'Telur Asin Asap Spesial', 'Telur yang setelah mengalami proses pengasinan di asapi di atas tungku api         ', 1000, '', '', 'Telor Asin Asap', 36000, 0, 'telorasinasapspesial.jpg'),
(19, 'Telur Asin Panggang Spesial', 'Telur yang setelah mengalami proses pengasinan lalu di panggang di dalam oven         ', 1000, '', '', 'Telor Asin Panggang', 36000, 0, 'telorasinpanggangspesial.jpg'),
(20, 'Telur Asin Rebus Spesial', 'Telur yang setelah mengalami proses pengasinan lalu di rebus', 1000, '', '', 'Telor Asin Rebus', 34000, 0, 'telorasinrebusspesial.jpg'),
(26, 'Telur Asin Asap', 'Telur yang setelah mengalami proses pengasinan di asapi di atas tungku api         ', 200, '', '', 'Telor Asin Asap', 3700, 0, 'telurasinasap.png'),
(27, 'Telur Asin Panggang', 'Telur yang setelah mengalami proses pengasinan lalu di panggang di dalam oven', 200, '', '', 'Telor Asin Panggang', 3700, 0, 'telorasinpanggang.png'),
(28, 'Telur Asin Rebus', 'Telur yang setelah mengalami proses pengasinan lalu di rebus', 200, '', '', 'Telor Asin Rebus', 3500, 0, 'telorasinrebus.png');

-- --------------------------------------------------------

--
-- Table structure for table `downloadtbl`
--

CREATE TABLE `downloadtbl` (
  `id` int(11) NOT NULL,
  `judul` text NOT NULL,
  `deskripsi` text NOT NULL,
  `url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `downloadtbl`
--

INSERT INTO `downloadtbl` (`id`, `judul`, `deskripsi`, `url`) VALUES
(1, 'Katalog Produk 2015', 'Informasi mengenai harga terbaru', 'download/KATALOG.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `kategoritbl`
--

CREATE TABLE `kategoritbl` (
  `id` int(11) NOT NULL,
  `kategori` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategoritbl`
--

INSERT INTO `kategoritbl` (`id`, `kategori`) VALUES
(5, 'Telor Asin Rebus'),
(6, 'Telor Asin Asap'),
(7, 'Telor Asin Panggang');

-- --------------------------------------------------------

--
-- Table structure for table `konfirmasitbl`
--

CREATE TABLE `konfirmasitbl` (
  `no_order` varchar(10) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `status` enum('sudah','belum') NOT NULL,
  `metode` varchar(255) NOT NULL,
  `jumlah_pembayaran` int(11) NOT NULL,
  `bank_asal` varchar(255) NOT NULL,
  `no_rekening_asal` int(15) NOT NULL,
  `waktu_pembayaran` date NOT NULL,
  `bank_tujuan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konfirmasitbl`
--

INSERT INTO `konfirmasitbl` (`no_order`, `nama`, `status`, `metode`, `jumlah_pembayaran`, `bank_asal`, `no_rekening_asal`, `waktu_pembayaran`, `bank_tujuan`) VALUES
('52', 'andriyas efendi', 'sudah', 'internet_banking', 250000, 'bca', 2147483647, '2015-01-01', 'mandiri');

-- --------------------------------------------------------

--
-- Table structure for table `newstbl`
--

CREATE TABLE `newstbl` (
  `tanggal` date NOT NULL,
  `judul` varchar(50) NOT NULL,
  `news` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newstbl`
--

INSERT INTO `newstbl` (`tanggal`, `judul`, `news`) VALUES
('2015-09-30', 'Diskon', 'Ada Diskon Lho');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggantbl`
--

CREATE TABLE `pelanggantbl` (
  `id` int(11) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `alamat` text NOT NULL,
  `email` text NOT NULL,
  `telepon` varchar(25) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggantbl`
--

INSERT INTO `pelanggantbl` (`id`, `nama`, `alamat`, `email`, `telepon`, `username`, `password`) VALUES
(15, 'Rian', 'Mancasan Kidul', 'rian@gmail.com', '087772003123', 'rian', 'cb2b28afc2cc836b33eb7ed86f99e65a'),
(16, 'andre', 'concat', '', '', '', 'd41d8cd98f00b204e9800998ecf8427e'),
(17, 'imron', 'tegal', 'imrontegal@gmail.com', '0856482', 'imron', '1aaec45698dc7574b81077fd7192110b'),
(18, 'Abdul', 'Jalan M Husni Thamrin 11 Tegal', 'abdulbrebes@gmail.com', '085678471745', 'Abdul', 'caff97b811b4c26e9f6c0eb4b67d1120'),
(19, 'Ahmad', 'Seturan', 'ahmadseturan@gmail.com', '08976586544', 'ahmad99', '724fabdfc98430d57a810787c4150f5a'),
(20, 'andriyas efendi', 'puluhdadi', 'efendiandriyas@gmail.com', '082341823', 'andri', '6bd3108684ccc9dfd40b126877f850b0'),
(21, 'asd', 'asdf', 'asdf@gmail.com', 'asd', 'asdf', '912ec803b2ce49e4a541068d495ab570');

-- --------------------------------------------------------

--
-- Table structure for table `transaksirincitbl`
--

CREATE TABLE `transaksirincitbl` (
  `notransaksi` varchar(25) NOT NULL,
  `username` varchar(20) NOT NULL,
  `id` int(11) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksirincitbl`
--

INSERT INTO `transaksirincitbl` (`notransaksi`, `username`, `id`, `nama`, `harga`, `jumlah`, `subtotal`) VALUES
('42', 'ahmad99', 16, 'Telur Asin Panggang', 3500, 1, 3500),
('44', 'ahmad99', 14, 'Telur Asin Rebus', 3000, 15, 45000),
('45', 'ahmad99', 16, 'Telur Asin Panggang', 3500, 2, 7000),
('45', 'ahmad99', 15, 'Telor Asin Asap', 3300, 1, 3300),
('45', 'ahmad99', 14, 'Telur Asin Rebus', 3000, 1, 3000),
('46', 'ahmad99', 14, 'Telur Asin Rebus', 3000, 1, 3000),
('47', 'ahmad99', 17, 'Telur Asin Rebus Special', 29000, 9, 261000),
('48', 'ahmad99', 17, 'Telur Asin Rebus Special', 29000, 1, 29000),
('49', 'ahmad99', 28, 'Telur Asin Rebus', 3500, 1, 3500),
('50', 'ahmad99', 28, 'Telur Asin Rebus', 3500, 1, 3500),
('51', 'ahmad99', 27, 'Telur Asin Panggang', 3700, 1, 3700),
('52', 'andri', 28, 'Telur Asin Rebus', 3500, 3, 10500),
('52', 'andri', 27, 'Telur Asin Panggang', 3700, 1, 3700),
('52', 'andri', 19, 'Telur Asin Panggang Spesial', 36000, 2, 72000),
('53', 'andri', 27, 'Telur Asin Panggang', 3700, 5, 18500),
('54', 'andri', 26, 'Telur Asin Asap', 3700, 1, 3700),
('55', 'andri', 27, 'Telur Asin Panggang', 3700, 1, 3700),
('56', 'andri', 27, 'Telur Asin Panggang', 3700, 1, 3700);

-- --------------------------------------------------------

--
-- Table structure for table `transaksitbl`
--

CREATE TABLE `transaksitbl` (
  `notransaksi` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `status_transaksi` enum('Belum Dibayar','Proses Pengiriman') NOT NULL,
  `alamat` text NOT NULL,
  `jasa` varchar(255) NOT NULL,
  `paket` varchar(255) NOT NULL,
  `biaya_kirim` int(11) NOT NULL,
  `total_biaya` int(11) NOT NULL,
  `noresi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksitbl`
--

INSERT INTO `transaksitbl` (`notransaksi`, `username`, `status`, `status_transaksi`, `alamat`, `jasa`, `paket`, `biaya_kirim`, `total_biaya`, `noresi`) VALUES
(52, 'andri', 1, 'Proses Pengiriman', ' , Yogyakarta , DI Yogyakarta , 55000', 'JNE', 'CTCSPS', 32000, 118200, 'afdafasdf2341234'),
(53, 'andri', 1, 'Belum Dibayar', ' , Yogyakarta , DI Yogyakarta , 55000', 'JNE', 'CTCOKE', 4000, 22500, ''),
(54, 'andri', 1, 'Belum Dibayar', 'aku mah apa atuh , Yogyakarta , DI Yogyakarta , 55000', 'JNE', 'CTCOKE', 4000, 7700, ''),
(55, 'andri', 1, 'Belum Dibayar', 'adfadsf , Yogyakarta , DI Yogyakarta , 55000', 'JNE', 'CTC', 5000, 8700, 'fhhfdhfd143245'),
(56, 'andri', 1, 'Belum Dibayar', ' , Sleman , DI Yogyakarta , 55500', 'JNE', 'OKE', 10000, 13700, '12341234afasdfasdf');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admintbl`
--
ALTER TABLE `admintbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barangtbl`
--
ALTER TABLE `barangtbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `downloadtbl`
--
ALTER TABLE `downloadtbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategoritbl`
--
ALTER TABLE `kategoritbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `konfirmasitbl`
--
ALTER TABLE `konfirmasitbl`
  ADD PRIMARY KEY (`no_order`);

--
-- Indexes for table `newstbl`
--
ALTER TABLE `newstbl`
  ADD UNIQUE KEY `judul` (`judul`);

--
-- Indexes for table `pelanggantbl`
--
ALTER TABLE `pelanggantbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksitbl`
--
ALTER TABLE `transaksitbl`
  ADD PRIMARY KEY (`notransaksi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admintbl`
--
ALTER TABLE `admintbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `barangtbl`
--
ALTER TABLE `barangtbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `downloadtbl`
--
ALTER TABLE `downloadtbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kategoritbl`
--
ALTER TABLE `kategoritbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `pelanggantbl`
--
ALTER TABLE `pelanggantbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `transaksitbl`
--
ALTER TABLE `transaksitbl`
  MODIFY `notransaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
